class ConfigError(Exception):
    """
    Base class for configuration Errors.
    """

    pass


class MissingFieldConfigError(ConfigError):
    """
    Error class which is thrown if a required field is missing in the config.
    """
    pass


class WrongTypeConfigError(ConfigError):
    """
    Error class which is thrown if a field has a value with the wrong type.
    """
    pass
