from neuroracer_common.third_party.profilehooks import timecall, coverage as cov, profile as prof

# verbose value of 2 and greater will apply the decorators
VERBOSE_DECORATOR = 2


def measure_time(immediate=True, timer=None, verbose=0):
    """
    TODO

    :param immediate:
    :type immediate:
    :param timer:
    :type timer:
    :param verbose:
    :type verbose:
    :return:
    """

    def wrapper(fn):
        if verbose >= VERBOSE_DECORATOR:
            return timecall(fn, immediate=immediate, timer=timer)

        else:
            return fn

    return wrapper


def coverage(verbose=0):
    """
    TODO

    :param verbose:
    :type verbose:
    :return:
    """

    def wrapper(fn):
        if verbose >= VERBOSE_DECORATOR:

            return cov(fn)
        else:
            return fn

    return wrapper


def profile(skip=0, filename=None, immediate=False, dirs=False,
            sort=None, entries=40,
            profiler=('cProfile', 'profile', 'hotshot'),
            stdout=True, verbose=0):
    """
    TODO

    :param skip:
    :type skip:
    :param filename:
    :type filename:
    :param immediate:
    :type immediate:
    :param dirs:
    :type dirs:
    :param sort:
    :type sort:
    :param entries:
    :type entries:
    :param profiler:
    :type profiler:
    :param stdout:
    :type stdout:
    :param verbose:
    :type verbose:
    :return:
    """

    def wrapper(fn):
        if verbose >= VERBOSE_DECORATOR:
            return prof(fn, skip=skip, filename=filename, immediate=immediate, dirs=dirs,
                        sort=sort, entries=entries,
                        profiler=profiler,
                        stdout=stdout)
        else:
            return fn

    return wrapper
