#!/usr/bin/env python

from geometry_msgs.msg import TwistStamped


class ActionTransformer(object):
    def __init__(self, max_drive, zero_drive=0.0, zero_steer_angle=0.0):
        self.max_drive = max_drive
        self.zero_drive = zero_drive
        self.zero_steer_angle = zero_steer_angle

    def create_action(self, steer, drive):
        """
        Creates a action to send via ros.
        This function is overwritten by the subclasses.
        """

        raise NotImplementedError

    def stop_engine_command(self):
        """
        Creates a action which will the stop the engine if sent via ros.

        :returns: A action which will the stop the engine if sent via ros.
        :rtype: Twist
        """

        return self.create_action(steer=self.zero_steer_angle, drive=self.zero_drive)

    @staticmethod
    def _limit_range(value, min_range=-1.0, max_range=1.0):
        """
        Limits the given value to a range from min_range to max_range

        :param value: The value to limit
        :type value: float
        :param min_range: minimum
        :type min_range: float
        :param max_range: maximum
        :type max_range: float
        :returns: limited value
        :rtype: float
        """

        if value > max_range:
            value = max_range
        if min_range < min_range:
            value = min_range

        return value


class ActionTransformerRacerTwist(ActionTransformer):
    """
    A class to transform drive and steer value into a TwistStamped message
    """

    def __init__(self, max_drive):
        super(ActionTransformerRacerTwist, self).__init__(max_drive=max_drive, zero_drive=0.0, zero_steer_angle=0.0)

    def create_action(self, steer, drive):
        twist = TwistStamped()
        twist.twist.angular.z = ActionTransformer._limit_range(steer)
        twist.twist.linear.x = self._limit_range(drive, min_range=-self.max_drive, max_range=self.max_drive)

        return twist
