"""
Provides a rospy Subscriber wrapper to dynamically adapt the buffer_size.

WARNING:
    When the first message comes in and the buffer_size if insufficient, the 
    topic is resubscribed. During the process of resubscribing there is a
    period when no callbacks are generated.
    The duration of this period depends on the new buffer_size.
    For a buffer_size of 100MB there is a period of about 0.1 seconds.
"""

import sys

import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import CompressedImage, Image, PointCloud, PointCloud2

import type_solving

cv_bridge = CvBridge()


def compressed_image_size_function(message):
    """
    Returns the size of the uncompressed image.

    :param message: A message of type CompressedImage
    :type message: CompressedImage

    :returns: The size of the uncompressed image.
    :rtype: int
    """

    image = cv_bridge.compressed_imgmsg_to_cv2(message)
    return image.nbytes


def image_size_function(message):
    """
    Returns the size of the image message.

    :param message: A message of type Image
    :type message: Image

    :returns: The size of the image.
    :rtype: int
    """

    # 200 byte for header and other slots
    safety_zone = 200
    return len(message.data) + safety_zone


def default_size_function(message):
    """
    Iterates recursive over the elements of message and determines its size.

    :param message: A ros message
    :type message: Message
    :returns: The size of the message
    :rtype: int
    """

    return sys.getsizeof(message)


#################################################################
# TYPE_TO_SIZE_FUNCTION:
#
# Use this dict to transform data types into functions, which are
# used to translate the types into the size of the message.
#################################################################

TYPE_TO_SIZE_FUNCTION = {
        CompressedImage: compressed_image_size_function,
        Image: image_size_function
    }


def _calculate_message_size(message, message_type):
    """
    Calculates the maximal message size from a message like message.
    For CompressedImage it will return the uncompressed size.

    :param message: The message of which the size is calculated.
    :type message: rospy.Message

    :param message_type: The type of the message.
    :type message_type: types.TypeType

    :returns: The minimal buffer_size needed for this message in bytes
    :rtype: int
    """

    if message_type in TYPE_TO_SIZE_FUNCTION:
        return TYPE_TO_SIZE_FUNCTION[message_type](message)
    return default_size_function(message)


def default_buffer_reallocation_size_function(minimal_buffer_size):
    """
    Default function calculating the next buffer_size if the old buffer_size was insufficient.

    :param minimal_buffer_size: The minimal_buffer_size needed for the biggest message.
    :type minimal_buffer_size: int
    :returns: The buffer_size which is allocated, if the current buffer_size is not sufficient
    :rtype: int
    """

    return minimal_buffer_size * 2


######################################################
# BIG_MESSAGE_TYPES:
#
# This list contains all ros Message Types, which are
# potentially to big for a 65536 bytes input buffer.
######################################################

BIG_MESSAGE_TYPES = [CompressedImage, Image, PointCloud, PointCloud2]


class AdaptiveSubscriber:
    """
    Wraps the rospy.Subscriber and adapts the buffer_size at runtime.
    """

    def __init__(self, name, data_class=None, callback=None, buffer_size=65536,
                 buffer_reallocation_size_function=default_buffer_reallocation_size_function):
        """
        Creates a AdaptiveSubscriber with the given callback.
        This callback will be called, if a message is received on this topic.
        The buffer_size of this this Subscriber is updated at runtime.

        :param name: The name of the topic.
        :type name: str

        :param data_class: Data type class to use for messages, e.g. std_msgs.msg.String.
                           If None the Subscriber tries to figure out the message type.
        :type data_class: Message

        :param callback: This callback is called, when a message is received.
        :type callback: Callback

        :param buffer_size: The initial buffer_size.
        :type buffer_size: int

        :param buffer_reallocation_size_function: A function that calculates the size of the new
                                                  buffer, if the old had insufficient space.
                                                  Gets the minimal needed buffer_size as argument.
        :type buffer_reallocation_size_function: Func(int): int

        :raises IOError: If the data_class was not given and the
                         type of the topic could not be resolved.
        """

        self._topic_name = name

        # if data_class not given, try to resolve
        if data_class is None:
            data_class = type_solving.get_topic_type(name)
        self._data_class = data_class

        self._external_callback = callback
        self._buffer_size = buffer_size
        self._buffer_reallocation_size_function = buffer_reallocation_size_function

        # only has to update, if data_class is a big message type
        self._has_to_update = data_class in BIG_MESSAGE_TYPES

        self._subscriber = None
        self._create_subscriber()

    def unregister(self):
        """
        Unsubscribes from the topic.
        Additional calls will have no effect.
        self._subscriber will be None after this call.
        """

        if self._has_subscriber():
            self._subscriber.unregister()
        self._subscriber = None
        self._has_to_update = True

    def _update_buffer_size(self, buffer_size):
        """
        Updates the buffer_size and creates a new Subscriber.

        :param buffer_size: The new buffer_size
        :type buffer_size: int
        """

        print("new buffer_size: {}".format(buffer_size))

        self._buffer_size = buffer_size
        self._update_subscriber()
        self._has_to_update = False

    def _update_subscriber(self):
        """
        Creates a new rospy.Subscriber with the current _topic_name, _callback and _buffer_size.
        Unsubscribes the current subscriber if existing.
        """

        self.unregister()
        self._create_subscriber()

    def _create_subscriber(self):
        """
        Creates a new rospy.Subscriber with the current _topic_name, _callback and _buffer_size.
        """

        self._subscriber = rospy.Subscriber(name=self._topic_name,
                                            data_class=self._data_class,
                                            callback=self._intern_callback,
                                            buff_size=self._buffer_size)

    def _has_subscriber(self):
        """
        Returns True if there is a intern Subscriber otherwise False.

        :returns: True if there is a intern Subscriber otherwise False.
        :rtype: bool
        """

        if self._subscriber and self._subscriber is not None:
            return True
        return False

    def _intern_callback(self, message):
        """
        This callback is called, if a message is received.
        This calls self._external_callback and modifies the buffer_size of self._subscriber.
        """

        self._external_callback(message)

        if self._has_to_update:
            needed_buffer_size = _calculate_message_size(message, self._data_class)
            if needed_buffer_size > self._buffer_size:
                self._update_buffer_size(self._buffer_reallocation_size_function(needed_buffer_size))

    def get_num_connections(self):
        """
        Returns the number of publishers to this topic.

        :returns: The number of publishers to this topic or 0 if
                  this subscriber has unsubscribed from the topic.
        """

        if self._has_subscriber():
            return self._subscriber.get_num_connections()
        return 0
