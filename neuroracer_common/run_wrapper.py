import os

import rospy
from neuroracer_configserver.config_server import run

"""
NeuroRace shared ROS-Nodes-API

"""


def run_ai():
    """
    Starts the ai_wrapper
    """

    from ai import AIWrapper

    try:
        ai_wrapper = AIWrapper.from_configserver("ai_wrapper")
        ai_wrapper.run()
    except rospy.ROSInterruptException:
        pass


def run_collector():
    """
    Launches Collector Node.
    """

    from collector import Collector

    try:
        collector = Collector.from_configserver("collector")
        collector.run()
    except rospy.ROSInterruptException:
        pass


def run_configserver(args):
    """
    Launches a config server.

    :param args: TODO
    :type args: TODO
    """

    if len(args) >= 2:
        config_base_path = os.path.expanduser(args[1])
    else:
        raise ValueError("Please specify a config_base_path via commandline")

    if not os.path.isabs(config_base_path):
        file_dir = os.path.dirname(__file__)
        config_base_path = os.path.normpath(os.path.join(file_dir, config_base_path))

    run(config_base_path)


def run_operator():
    """
    Launches Operator Node.
    """

    from operator import Operator

    operator = Operator.from_configserver(["operator", "joy_mapping"])
    operator.run()
