# Common Package
Collection of shared features between real world robot and simulation

## On this page

- [Requirements](#requirements)
- [Python Version](#python-version)
- [Installation](#installation)
- [Backend](#backend)
- [Content](#content)
  - [Operator (Controls)](#operator-controls)
  - [Data Collector](#data-collector)
  - [Adaptive Subscriber](#adaptive-subscriber)
  - [Python Import Solver](#python-import-solver)
  - [Decorators](#decorators)
    - [Time Measurement](#time-measurement)
    - [Coverage](#coverage)
    - [Profile](#profile)
  - [Run Wrappers](#run-wrappers)

## Requirements
The `neuroracer-common` is a non-standalone package and is designed to be used within the NeuroRace framework. It 
`requires` the [`neuroracer-configserver`](https://gitlab.com/NeuroRace/neuroracer-configserver.git) as dependency.

## Python Version
For extracting data from [ROS bags](http://wiki.ros.org/Bags), due to [ROS](http://wiki.ros.org/Documentation), Python 2.7 is required. Tasks not requiring ROS support both, Python2.7 and Python3.x.   

As soon as ROS fully supports Python3.x, the Python2.7 support will be dropped.

## Installation
Install `neuroracer-common` package via pip. Therefor clone this repository and install the package.

```
git clone https://gitlab.com/NeuroRace/neuroracer-common.git
pip2 install --user -e neuroracer-common[backend]
pip3 install --user -e neuroracer-common[backend]
```

where `backend` can be either [Keras](https://github.com/keras-team/keras) with [Tensorflow](https://github.com/tensorflow/tensorflow) or [PyTorch](https://github.com/pytorch/pytorch). The available install options are
* `keras_tf` (Keras with Tensorflow backend - cpu only)
* `keras_tf_gpu` (Keras with Tensorflow backend - gpu support)
* `torch` (dynamically selects cpu/gpu)

So for example if you want to install the PyTorch backend for python3.x, the command would be
```
pip3 install --user -e neuroracer-common[torch]
```

Of course you can install Keras and PyTorch side by side and switch via config file any time. See [Backend](#backend) section for further information.

## Backend
To switch the used backend, edit `$HOME/.neurorace/neuroracer.yaml`

* Keras
```yaml
{backend: keras}
```

PyTorch
```yaml
{backend: pytorch}
```

**Note:**  
The folder and config file will be created on first execution with keras as default.

## Content
TODO

### Operator (Controls)
TODO

### Data Collector
TODO

### Adaptive Subscriber
TODO

### Python Import Solver
TODO py2/3 imports different, etc.

### Decorators
TODO

#### Time Measurement
TODO

#### Coverage
TODO

#### Profile
TODO

### Run Wrappers
Wrapper functions to run
* AI
* Collector
* Configserver
* Operator
